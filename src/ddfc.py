from node import *

FILE_DATA = 'data/data100.txt'
TIMER_EXPIRE = 10

DIR_FILES = 'data' 
files_id = [
             [1, [2,3]], 
             [2, [1,3,33]], 
             [3, [1,2]], 
             [33, [2,35]], 
             [35, [33]]
             ]

lines = {}
nodes = []

n1 = Node()
n1.id=1

n2 = Node()
n2.id = 2

n3 = Node()
n3.id = 3

n33 = Node()
n33.id = 33

n35 = Node()
n35.id = 35

nodes = [n1,n2,n3,n33,n35]

n1.vizinhos_fisicos.append(n2)
n1.vizinhos_fisicos.append(n3)

n2.vizinhos_fisicos.append(n1)
n2.vizinhos_fisicos.append(n3)
n2.vizinhos_fisicos.append(n33)

n3.vizinhos_fisicos.append(n1)
n3.vizinhos_fisicos.append(n2)

n33.vizinhos_fisicos.append(n2)
n33.vizinhos_fisicos.append(n35)

n35.vizinhos_fisicos.append(n33)

# Ler linhas dos arquivos
for node in nodes:
    f_id = str(node.id)
    file_name = DIR_FILES + '/' + f_id
    arquivo = open(file_name)
    linhas = arquivo.readlines()
    
    node.lines = linhas
    
    arquivo.close()

cont_timer = 0

while (cont_timer < 990):
     
    cont_timer = cont_timer + 1
    if (cont_timer % TIMER_EXPIRE == 0):
        
        #Simular que a mensagem de um no sera recebida por todos os outros nos
        for node_tran in nodes: #no que vai transmitir
            linhas = node_tran.lines
            rnd = cont_timer - 1
            linha = linhas[rnd]
            linha = linha.split()
            reading_tran = float(linha[4])
            average_reanding_tran = node_tran.get_average_reading(rnd)
            number_sneigh = len(node_tran.sNeigh)
            
            for node_rec in node_tran.vizinhos_fisicos: #no que vai receber 
                node_rec.receive_beacon(node_tran, reading_tran, average_reanding_tran, number_sneigh, rnd)
        
        for node in nodes:
            print(node)
        
        print("")