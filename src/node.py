from NeighR import *

class Node:
    def __init__(self):
        self.id = 0
        self.sNeigh = []
        self.neighRs = {}
        self.lines = []
        self.cthresh = 3
        self.vizinhos_fisicos = []
    
    def get_average_reading(self, rnd):
        line = self.lines[rnd]
        line = line.split()
        accumulated = float(line[4]) # 4 coluna
        number_of_readings = 1
        
        for node in self.sNeigh:
            neigh_r = self.neighRs[node.id]
            temp = neigh_r.aggregated_readings * neigh_r.numeber_of_aggrefated
            accumulated = accumulated + temp
            number_of_readings = number_of_readings + neigh_r.numeber_of_aggrefated
        
        return accumulated / float(number_of_readings)
    
    def receive_beacon(self, node, reading, accumulated, number_readings, rnd):
        neigh_r = NeighR()
        neigh_r.individual_reading = reading
        neigh_r.aggregated_readings = accumulated
        neigh_r.numeber_of_aggrefated = number_readings
          
        self.neighRs[node.id] = neigh_r
        
        local_avg = self.get_average_reading(rnd)
        
        line = self.lines[rnd]
        line = line.split()
        local_reading = float(line[4])
        
        if ((abs(reading - local_avg) < self.cthresh) and ((local_reading-accumulated) < self.cthresh)):
            if (node not in self.sNeigh):
                self.sNeigh.append(node)
        else:
            if (node in self.sNeigh):
                self.sNeigh.remove(node)
    
    def __str__(self):
        return ("%s: %s" % (self.id, [node.id for node in self.sNeigh]))
        
        